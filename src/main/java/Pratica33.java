import utfpr.ct.dainf.if62c.pratica.Matriz;

/**
 * IF62C Fundamentos de Programação 2
 * Exemplo de programação em Java.
 * @author Wilson Horstmeyer Bogado <wilson@utfpr.edu.br>
 */
public class Pratica33 {

    public static void main(String[] args) {
        Matriz orig = new Matriz(3, 2);
        double[][] m = orig.getMatriz();
        m[0][0] = 0.0;
        m[0][1] = 0.1;
        m[1][0] = 1.0;
        m[1][1] = 1.1;
        m[2][0] = 2.0;
        m[2][1] = 2.1;
        Matriz transp = orig.getTransposta();
        System.out.println("Matriz original: " + orig);
        System.out.println("Matriz transposta: " + transp);


        Matriz nova = new Matriz(3, 2);
        double[][] _nova = nova.getMatriz();
        _nova[0][0] = 1.0;
        _nova[0][1] = 2.0;
        _nova[1][0] = 3.0;
        _nova[1][1] = 4.0;
        _nova[2][0] = 5.0;
        _nova[2][1] = 6.0;
        Matriz somada = orig.soma(nova);
        System.out.println("Matriz nova: " + nova);
        System.out.println("Matriz somada: " + somada);


        orig = new Matriz(3, 2);
        m = orig.getMatriz();
        m[0][0] = 2.0;
        m[0][1] = 3.0;
        m[1][0] = 0.0;
        m[1][1] = 1.0;
        m[2][0] = -1.0;
        m[2][1] = 4.0;

        nova = new Matriz(2, 3);
        _nova = nova.getMatriz();
        _nova[0][0] = 1.0;
        _nova[0][1] = 2.0;
        _nova[0][2] = 3.0;
        _nova[1][0] = -2.0;
        _nova[1][1] = 0.0;
        _nova[1][2] = 4.0;
        Matriz multiplicada = orig.prod(nova);
        System.out.println("Matriz original: " + orig);
        System.out.println("Matriz nova: " + nova);
        System.out.println("Matriz multiplicada AxB: " + multiplicada);
        multiplicada = nova.prod(orig);
        System.out.println("Matriz multiplicada BxA: " + multiplicada);
    }
}
